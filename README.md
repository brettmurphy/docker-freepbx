# FreePBX on Docker

### Image includes

 * phusion/baseimage:0.9.18 (Ubuntu 14.04)
 * LAMP stack (apache2, mysql, php)
 * Asterisk 13.15.1
 * FreePBX 13.0.192.13
 


### Run your FreePBX image
```bash
docker run --net=host -d -t murf66/freepbx
```

Test it out by visiting your hosts ip address in a browser.

